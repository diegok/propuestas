# Nombre del taller

Taller de observabilidad en tu malla de servicios con with Istio, Kiali y MiniKube

## Objetivos a cubrir en el taller

Los asistentes montaran un entorno de microservicios con minikube y lo securizaran con Istio, se ejecutaran las demos de istio.io como el bookinfo y se trabajara con el entorno y se visualizara con Kiali

## Público objetivo

Estudiantes de FP, Universitari@s, Profesionales o cualquier usuari@ interesad@ en la seguridad en microservicios

## Ponente(s)

Ingeniero Senior en RedHat trabajando en ManageIQ, Istio y Kiali con Ror, Go, Angular y React. Anteriomente trabajando en BBVA como Arquitecto de sistemas en Hadoop y desarrollador, Alberto trabaja ahora desarrollando y manteniendo proyectos de codigo libre.

Esta charla se ha dado en la CommitConf y Codemotion

### Contacto(s)

Alberto Jesus Gutierrez Juanes: aljesusg @ gmail


## Prerequisitos para los asistentes

### Requisitos Laptop
Requisitos minimo minikube https://kubernetes.io
Requisitos minimos Istio https://istio.io/

### Conocimientos mínimos
Sabes que es un container, pod, servicio etc... conomientos minimos de kubernetes

## Prerequisitos para la organización

Ninguno

## Tiempo

Dos horas, ya que pueden surgir dificultades dependiendo del portatil del asistente.

## Día

Indiferente

## Comentarios

Ninguno
## Condiciones

* [X] Acepto seguir el [código de conducta](https://eslib.re/2019/conducta/).
* [X] Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
* [X] Acepto coordinarme con la organización de esLibre.


