# Proyectos educativos para aprender programación y robótica

Hace 5 años que se creó la Asociación Programo Ergo Sum destinada a la enseñanza de la informática, programación, electrónica y robótica en los centros educativos utilizando herramientas de software y hardware libre. Tras 5 intensos años disponemos de varios proyectos libres y gratuitos los cuales reciben más de 7mil visitas diarias de todas las partes del mundo.

## Formato de la propuesta

Indicar uno de estos:

* [X] Charla (25 minutos)
* [ ] Charla relámpago (10 minutos)

## Descripción

[Programo Ergo Sum](https://www.programoergosum.es) es una asociación sin ánimo de lucro que promueve la enseñanza de la programación y la robótica en los centros educativos utilizando herramientas libres.

La Asociación cuenta en estos momentos con 3 proyectos educativos libres y gratuitos al 100%:

* La [Plataforma de tutoriales](https://www.programoergosum.es/tutoriales) contiene más de 100 tutoriales sobre informática, programación, electrónica y robótica educativa. Todos los tutoriales están alojados en un [repositorio público en GitHub](https://github.com/ProgramoErgoSum/Tutoriales) para que cualquier entusiasta de la educación pueda aportar su granito de arena. Se respeta siempre la autoría de los contribuyentes apareciendo su nombre e imagen de GitHub en la plataforma. Además, también la propia [página web está alojada en GitHub](https://github.com/ProgramoErgoSum/www.programoergosum.es) para que cualquier programador con conocimientos en programación Web pueda contribuir y hacer una educación más accesible.

* Otro proyecto educativo es la [Plataforma Aprende Programando](https://www.aprendeprogramando.es/) destinada a la enseñanza de la programación desde educación secundaria con el objetivo de resolver retos de programación relacionados con las matemáticas, física y tecnología. Los retos están orientados para poder continuar realizando olimpiadas de programación para alumnos de la ESO y Bachiller. Este proyecto aunque es gratuito todavía no está alojado el código fuente en repositorios públicos, pero calculamos que de cara al próximo curso escolar tendremos la nueva versión que sí estará alojada en GitHub en repositorios públicos.

* El proyecto educativo de [El Cable Amarillo](https://www.elcableamarillo.cc/) surge ante la necesidad de que los docentes de la Región de Murcia pudieran utilizar un material de robótica con placas de Arduino. La idea inicial consiste en alojar prácticas que puedan utilizarse en los niveles de educación primaria, secundaria o bachiller y tener un almacen de prácticas para los docentes. Son muchos los docentes los que han incluido sus prácticas. Tanto la Web como el repositorio de prácticas se encuentran alojadas en [GitHub de El Cable Amarillo](https://github.com/ElCableAmarillo).

En esta charla se explicará además como contribuir realizando sencillos Pull-Requests a los proyectos mencionados.

## Público objetivo

Dirigido a docentes interesados en utilizar herramientas libres para aprender informática, programación, electrónica o robótica en los centros educativos, así como a cualquier persona que quiera iniciarse desde cero.

## Ponente(s)

¿Quién o quienes van a dar la charla? ¿Qué hacen? ¿Qué charlas han
dado antes?
[Miguel Ángel Abellán](https://migueabellan.github.io). Sí he dado varias charlas sobre el software libre y la enseñanza de la programación en los centros educativos.

### Contacto(s)

* Nombre: Miguel Ángel Abellán
* Contacto: [info@migueabellan.es](mailto:info@migueabellan.es) @migueabellan

## Comentarios

El objetivo de la charla es dar a conocer todos los proyectos educativos libres que desarrollamos desde la asociación y animar a los docentes y público en general cómo pueden aportar su granito de arena contribuyendo a los repositorios.

## Condiciones

* [X] Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [X] Al menos una persona entre los que la proponen estará presente el día programado para la charla.
